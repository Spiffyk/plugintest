#include <stdio.h>

#include "program_api.h"
#include "plugin_api.h"

void plugin_entry(void)
{
	fprintf(stderr, "my-plugin plugin_entry\n");
	program_function();
}
