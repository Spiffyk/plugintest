#include <dlfcn.h>
#include <stdio.h>

#include "program_api.h"
#include "plugin_api.h"

void program_function(void)
{
	fprintf(stderr, "program_function\n");
}

int main(void)
{
	fprintf(stderr, "loading plugin 1\n");
	void *lib1 = dlopen("./libmy-plugin.so", RTLD_NOW);
	if (!lib1) {
		fprintf(stderr, "dlopen error: %s\n", dlerror());
		return 1;
	}

	fprintf(stderr, "getting symbol 1\n");
	plugin_entry_cb cb1 = dlsym(lib1, "plugin_entry");
	if (!cb1) {
		fprintf(stderr, "dlsym error: %s\n", dlerror());
		return 1;
	}

	fprintf(stderr, "loading plugin 2\n");
	void *lib2 = dlopen("./libmy-plugin2.so", RTLD_NOW);
	if (!lib2) {
		fprintf(stderr, "dlopen error: %s\n", dlerror());
		return 1;
	}

	fprintf(stderr, "getting symbol 2\n");
	plugin_entry_cb cb2 = dlsym(lib2, "plugin_entry");
	if (!cb2) {
		fprintf(stderr, "dlsym error: %s\n", dlerror());
		return 1;
	}

	fprintf(stderr, "calling plugin\n");
	cb1();
	cb2();

	fprintf(stderr, "closing plugin 1\n");
	int ret = dlclose(lib1);
	if (ret) {
		fprintf(stderr, "dlclose error: %s\n", dlerror());
		return 1;
	}

	fprintf(stderr, "closing plugin 2\n");
	ret = dlclose(lib2);
	if (ret) {
		fprintf(stderr, "dlclose error: %s\n", dlerror());
		return 1;
	}

	return 0;
}
